<?php
//areasConocimientoTableSeeder

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class areasConocimientoTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('areas_conocimiento')->truncate();

        $areas_conocimiento = [
            ["codigo"=>11,"nucleo_basico"=>"AGRONOMIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>1,"id"=>1],
            ["codigo"=>12,"nucleo_basico"=>"ZOOTECNIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>1,"id"=>2],
            ["codigo"=>13,"nucleo_basico"=>"MEDICINA VETERINARIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>1,"id"=>4],
            ["codigo"=>24,"nucleo_basico"=>"ARTES PLASTICAS, VISUALES Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>2,"id"=>5],
            ["codigo"=>270,"nucleo_basico"=>"OTROS PROGRAMAS ASOCIADOS A BELLAS ARTES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>2,"id"=>6],
            ["codigo"=>28,"nucleo_basico"=>"MUSICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>2,"id"=>8],
            ["codigo"=>25,"nucleo_basico"=>"ARTES REPRESENTATIVAS","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>2,"id"=>9],
            ["codigo"=>26,"nucleo_basico"=>"PUBLICIDAD Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>2,"id"=>10],
            ["codigo"=>27,"nucleo_basico"=>"DISENIO","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>2,"id"=>11],
            ["codigo"=>440,"nucleo_basico"=>"BACTERIOLOGIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>12],
            ["codigo"=>447,"nucleo_basico"=>"ODONTOLOGIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>16],
            ["codigo"=>450,"nucleo_basico"=>"SALUD PUBLICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>18],
            ["codigo"=>448,"nucleo_basico"=>"OPTOMETRIA, OTROS PROGFRAMAS DE CIENCIAS DE LA SALUD","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>19],
            ["codigo"=>446,"nucleo_basico"=>"NUTRICION Y DIETETICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>20],
            ["codigo"=>441,"nucleo_basico"=>"ENFERMERIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>21],
            ["codigo"=>442,"nucleo_basico"=>"TERAPIAS","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>22],
            ["codigo"=>445,"nucleo_basico"=>"MEDICINA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>24],
            ["codigo"=>444,"nucleo_basico"=>"INSTRUMENTACION QUIRURGICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>3,"id"=>27],
            ["codigo"=>553,"nucleo_basico"=>"ANTROPOLOGIA, ARTES LIBRERALES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>28],
            ["codigo"=>562,"nucleo_basico"=>"GEOGRAFIA, HISTORIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>29],
            ["codigo"=>569,"nucleo_basico"=>"SOCIOLOGIA, TRABAJO SOCIAL Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>30],
            ["codigo"=>568,"nucleo_basico"=>"FILOSOFIA, TEOLOGIA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>31],
            ["codigo"=>566,"nucleo_basico"=>"PSICOLOGIA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>32],
            ["codigo"=>564,"nucleo_basico"=>"LENGUAS MODERNAS, LITERATURA, LINGUISTICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>33],
            ["codigo"=>561,"nucleo_basico"=>"FORMACION RELACIONADA CON EL CAMPO MILITAR O POLICIAL","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>34],
            ["codigo"=>555,"nucleo_basico"=>"BIBLIOTECOLOGIA, OTROS DE CIENCIAS SOCIALES Y HUMANAS","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>36],
            ["codigo"=>557,"nucleo_basico"=>"COMUNICACION SOCIAL, PERIODISMO Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>37],
            ["codigo"=>558,"nucleo_basico"=>"DEPORTES, EDUCACION FISICA Y RECREACION","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>38],
            ["codigo"=>559,"nucleo_basico"=>"DERECHO Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>39],
            ["codigo"=>556,"nucleo_basico"=>"CIENCIA POLITICA, RELACIONES INTERNACIONALES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>4,"id"=>41],
            ["codigo"=>313,"nucleo_basico"=>"EDUCACION","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>5,"id"=>42],
            ["codigo"=>69,"nucleo_basico"=>"ADMINISTRACION","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>6,"id"=>43],
            ["codigo"=>612,"nucleo_basico"=>"CONTADURIA PUBLICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>6,"id"=>44],
            ["codigo"=>611,"nucleo_basico"=>"ECONOMICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>6,"id"=>45],
            ["codigo"=>934,"nucleo_basico"=>"BIOLOGIA, MICROBIOLOGIA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>7,"id"=>46],
            ["codigo"=>935,"nucleo_basico"=>"FISICA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>7,"id"=>47],
            ["codigo"=>936,"nucleo_basico"=>"GEOLOGIA, OTROS PROGRAMAS DE CIENCIAS NATURALES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>7,"id"=>48],
            ["codigo"=>939,"nucleo_basico"=>"QUIMICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>7,"id"=>49],
            ["codigo"=>937,"nucleo_basico"=>"MATEMATICAS, ESTADISTICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>7,"id"=>50],
            ["codigo"=>818,"nucleo_basico"=>"ARQUITECTURA","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>51],
            ["codigo"=>824,"nucleo_basico"=>"INGENIERIA, AGRONOMICA, PECUARIA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>52],
            ["codigo"=>826,"nucleo_basico"=>"INGENIERIA DE MINAS, METALURGIA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>54],
            ["codigo"=>833,"nucleo_basico"=>"OTRAS INGENIERIAS","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>56],
            ["codigo"=>832,"nucleo_basico"=>"INGENIERIA QUIMICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>57],
            ["codigo"=>831,"nucleo_basico"=>"INGENIERIA MECANICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>58],
            ["codigo"=>830,"nucleo_basico"=>"INGENIERIA INDUSTRIAL Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>60],
            ["codigo"=>829,"nucleo_basico"=>"INGENIERIA ELECTRONICA, TELECOMUNICACIONES Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>61],
            ["codigo"=>828,"nucleo_basico"=>"INGENIERIA ELECTRICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>62],
            ["codigo"=>827,"nucleo_basico"=>"INGENIERIA DE SISTEMAS, TELEMATICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>63],
            ["codigo"=>825,"nucleo_basico"=>"INGENIERIA CIVIL Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>65],
            ["codigo"=>823,"nucleo_basico"=>"INGENIERIA AGROINDUSTRIAL, ALIMENTOS Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>66],
            ["codigo"=>819,"nucleo_basico"=>"INGENIERIA BIOMEDICA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>68],
            ["codigo"=>820,"nucleo_basico"=>"INGENIERIA AMBIENTAL, SANITARIA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>69],
            ["codigo"=>822,"nucleo_basico"=>"INGENIERIA AGRICOLA, FORESTRAL Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>70],
            ["codigo"=>821,"nucleo_basico"=>"INGENIERIA ADMINISTRATIVA Y AFINES","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>8,"id"=>71],
            ["codigo"=>14,"nucleo_basico"=>"SIN CLASIFICAR","updated_at"=>"2018-03-12 14:48:31.0","created_at"=>"2018-03-12 14:48:31.0","areas_id"=>9,"id"=>72]
        ];


        DB::table('areas_conocimiento')->insert($areas_conocimiento);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}