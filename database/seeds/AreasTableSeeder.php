<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class AreasTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('areas')->truncate();

        $areas = [
            ["codigo"=>1,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>1,"nombre"=>"AGRONOMIA VETERINARIA Y AFINES"],
            ["codigo"=>2,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>2,"nombre"=>"BELLAS ARTES"],
            ["codigo"=>4,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>3,"nombre"=>"CIENCIAS DE LA SALUD"],
            ["codigo"=>5,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>4,"nombre"=>"CIENCIAS SOCIALES Y HUMANAS"],
            ["codigo"=>3,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>5,"nombre"=>"CIENCIAS DE LA EDUCACION"],
            ["codigo"=>6,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>6,"nombre"=>"ECONOMIA, ADMINISTRACION CONTADURIA Y AFINES"],
            ["codigo"=>9,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>7,"nombre"=>"MATEMATICAS Y CIENCIAS NATURALES"],
            ["codigo"=>8,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>8,"nombre"=>"INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES"],
            ["codigo"=>14,"updated_at"=>"2018-03-12 14:47:31.0","created_at"=>"2018-03-12 14:47:31.0","id"=>9,"nombre"=>"SIN CLASIFICAR"]
        ];


        DB::table('areas')->insert($areas);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
