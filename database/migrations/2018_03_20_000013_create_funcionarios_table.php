<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'funcionarios';

    /**
     * Run the migrations.
     * @table funcionarios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('correo')->nullable()->default(null);
            $table->string('documento', 11)->nullable()->default(null);
            $table->string('telefono',255)->nullable()->default(null);
            $table->unsignedInteger('tipo_functionarios_id');
            $table->unsignedInteger('centros_id');

            $table->index(["centros_id"], 'fk_funcionarios_centros1_idx');

            $table->index(["tipo_functionarios_id"], 'fk_funcionarios_tipo_functionarios1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('centros_id', 'fk_funcionarios_centros1_idx')
                ->references('id')->on('centros')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('tipo_functionarios_id', 'fk_funcionarios_tipo_functionarios1_idx')
                ->references('id')->on('tipo_functionarios')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
