<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentrosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'centros';

    /**
     * Run the migrations.
     * @table centros
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre')->default('');
            $table->unsignedInteger('regionales_id');
            $table->string('subdirector_nombre')->nullable()->default(null);
            $table->string('subdirector_celular')->nullable()->default(null);
            $table->string('subdirector_correo')->nullable()->default(null);
            $table->string('codigo');

            $table->index(["regionales_id"], 'fk_centros_regionales1_idx');

            $table->unique(["codigo"], 'centros_codigo_unique');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('regionales_id', 'fk_centros_regionales1_idx')
                ->references('id')->on('regionales')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
