<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineasRublosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'lineas_rublos';

    /**
     * Run the migrations.
     * @table lineas_rublos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('rublos_id');
            $table->unsignedInteger('lineas_id');

            $table->index(["lineas_id"], 'fk_lineas_rublos_lineas1_idx');

            $table->index(["rublos_id"], 'fk_lineas_rublos_rublos1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('lineas_id', 'fk_lineas_rublos_lineas1_idx')
                ->references('id')->on('lineas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rublos_id', 'fk_lineas_rublos_rublos1_idx')
                ->references('id')->on('rublos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
