<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'items';

    /**
     * Run the migrations.
     * @table items
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('nombre');
            $table->string('modelo')->default('');
            $table->text('descripcion');
            $table->unsignedBigInteger('placa');
            $table->string('serial')->nullable()->default(null);
            $table->date('fecha_aquisicion');
            $table->decimal('precio', 20, 2)->nullable()->default(null);
            $table->unsignedInteger('proyectos_id');
            $table->unsignedInteger('lineas_rublos_id');
            $table->integer('categorias_id');

            $table->index(["categorias_id"], 'fk_items_categorias1_idx');

            $table->index(["proyectos_id"], 'fk_items_proyectos1_idx');

            $table->index(["lineas_rublos_id"], 'fk_items_lineas_rublos1_idx');

            $table->unique(["placa"], 'placa_UNIQUE');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('lineas_rublos_id', 'fk_items_lineas_rublos1_idx')
                ->references('id')->on('lineas_rublos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('proyectos_id', 'fk_items_proyectos1_idx')
                ->references('id')->on('proyectos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
