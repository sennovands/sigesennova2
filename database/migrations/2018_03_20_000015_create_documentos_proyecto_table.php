<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosProyectoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'documentos_proyecto';

    /**
     * Run the migrations.
     * @table documentos_proyecto
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre');
            $table->string('formato', 10);
            $table->decimal('peso', 40, 2);
            $table->text('observaciones')->nullable()->default(null);
            $table->unsignedInteger('proyectos_id');

            $table->index(["proyectos_id"], 'fk_documentos_proyecto_proyectos1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('proyectos_id', 'fk_documentos_proyecto_proyectos1_idx')
                ->references('id')->on('proyectos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
