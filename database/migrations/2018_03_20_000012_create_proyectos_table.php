<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'proyectos';

    /**
     * Run the migrations.
     * @table proyectos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('centro_id')->nullable()->default(null);
            $table->string('subdirector_nombre')->nullable()->default(null);
            $table->string('subdirector_documento')->nullable()->default(null);
            $table->string('subdirector_celular')->nullable()->default(null);
            $table->integer('titulo')->nullable()->default(null);
            $table->text('autores')->nullable()->default(null);
            $table->integer('radico_funcionario_id')->nullable()->default(null);
            $table->unsignedInteger('lineas_id');
            $table->unsignedInteger('areas_conocimiento_id');
            $table->unsignedInteger('areas_conocimiento_id1');
            $table->string('estado', 45)->nullable()->default(null);

            $table->index(["lineas_id"], 'fk_proyectos_lineas1_idx');

            $table->index(["centro_id"], 'centro_id');

            $table->index(["areas_conocimiento_id"], 'fk_proyectos_areas_conocimiento1_idx');

            $table->index(["areas_conocimiento_id1"], 'fk_proyectos_areas_conocimiento2_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('centro_id', 'centro_id')
                ->references('id')->on('centros')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('areas_conocimiento_id', 'fk_proyectos_areas_conocimiento1_idx')
                ->references('id')->on('areas_conocimiento')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('areas_conocimiento_id1', 'fk_proyectos_areas_conocimiento2_idx')
                ->references('id')->on('areas_conocimiento')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('lineas_id', 'fk_proyectos_lineas1_idx')
                ->references('id')->on('lineas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
