<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasConocimientoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'areas_conocimiento';

    /**
     * Run the migrations.
     * @table areas_conocimiento
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('codigo')->nullable()->default(null);
            $table->string('nucleo_basico')->nullable()->default(null);
            $table->unsignedInteger('areas_id');

            $table->index(["areas_id"], 'fk_areas_conocimiento_areas1_idx');

            $table->unique(["codigo"], 'areas_conocimiento_codigo_unique');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('areas_id', 'fk_areas_conocimiento_areas1_idx')
                ->references('id')->on('areas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
