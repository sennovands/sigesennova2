<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsFuncionariosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'items_funcionarios';

    /**
     * Run the migrations.
     * @table items_funcionarios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('fecha_asignado');
            $table->date('fecha_desasignado')->nullable()->default(null);
            $table->unsignedInteger('funcionarios_id');
            $table->unsignedInteger('items_id');

            $table->index(["funcionarios_id"], 'fk_items_funcionarios_funcionarios1_idx');

            $table->index(["items_id"], 'fk_items_funcionarios_items1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('funcionarios_id', 'fk_items_funcionarios_funcionarios1_idx')
                ->references('id')->on('funcionarios')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('items_id', 'fk_items_funcionarios_items1_idx')
                ->references('id')->on('items')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
