<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// RUTAS REGIONALES
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('regional', 'RegionalController');
Route::get('regional/delete/{id}','RegionalController@delete')->name('regional.delete');
Route::get('regional/restaure/{id}','RegionalController@restore')->name('regional.restore');
//RUTAS CENTROS
Route::resource('centros', 'CentrosController');
Route::get('centros/delete/{id}','CentrosController@delete')->name('centros.delete');
Route::get('centros/restaure/{id}','CentrosController@restore')->name('centros.restore');
//RUTAS RUBLOS
Route::resource('rublos', 'RublosController');
Route::get('rublos/delete/{id}','RublosController@delete')->name('rublos.delete');
Route::get('rublos/restaure/{id}','RublosController@restore')->name('rublos.restore');
//RUTAS LINEAS
Route::resource('lineas', 'LineasController');
Route::get('lineas/delete/{id}','LineasController@delete')->name('lineas.delete');
Route::get('lineas/restaure/{id}','LineasController@restore')->name('lineas.restore');
//AREAS DE CONOCIMIENTO
Route::get('/areas-conocimiento', 'AreasConocimientoController@index')->name('areas.index');
//RUTAS FUNCIONARIOS
Route::resource('funcionarios','FuncionarosController');
Route::get('funcionarios/delete/{id}','FuncionarosController@delete')->name('funcionarios.delete');
Route::get('funcionarios/restaure/{id}','FuncionarosController@restore')->name('funcionarios.restore');
//RUTAS PROYECTOS
Route::resource('proyectos','ProyectosController');
Route::get('proyectos/lista/{tipo}', 'ProyectosController@search')->name('proyectos.search');
Route::get('proyectos/delete/{id}','ProyectosController@delete')->name('proyectos.delete');
Route::get('proyectos/restaure/{id}','ProyectosController@restore')->name('proyectos.restore');