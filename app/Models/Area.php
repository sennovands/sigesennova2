<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Area
 *
 * @property int $id
 * @property int $codigo
 * @property string $nombre
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Illuminate\Database\Eloquent\Collection $areas_conocimientos
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Area onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Area withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Area withoutTrashed()
 * @mixin \Eloquent
 */
class Area extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'codigo' => 'int'
	];

	protected $fillable = [
		'codigo',
		'nombre'
	];

	public function areas_conocimientos()
	{
		return $this->hasMany(\App\Models\AreasConocimiento::class, 'areas_id');
	}
}
