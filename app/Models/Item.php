<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Item
 *
 * @property int $id
 * @property string $nombre
 * @property string $modelo
 * @property string $descripcion
 * @property int $placa
 * @property string $serial
 * @property \Carbon\Carbon $fecha_aquisicion
 * @property float $precio
 * @property int $proyectos_id
 * @property int $lineas_rublos_id
 * @property int $categorias_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\LineasRublo $lineas_rublo
 * @property \App\Models\Proyecto $proyecto
 * @property \Illuminate\Database\Eloquent\Collection $funcionarios
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Item onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereCategoriasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereFechaAquisicion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereLineasRublosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereModelo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item wherePlaca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereProyectosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Item withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Item withoutTrashed()
 * @mixin \Eloquent
 */
class Item extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'placa' => 'int',
		'precio' => 'float',
		'proyectos_id' => 'int',
		'lineas_rublos_id' => 'int',
		'categorias_id' => 'int'
	];

	protected $dates = [
		'fecha_aquisicion'
	];

	protected $fillable = [
		'nombre',
		'modelo',
		'descripcion',
		'placa',
		'serial',
		'fecha_aquisicion',
		'precio',
		'proyectos_id',
		'lineas_rublos_id',
		'categorias_id'
	];

	public function lineas_rublo()
	{
		return $this->belongsTo(\App\Models\LineasRublo::class, 'lineas_rublos_id');
	}

	public function proyecto()
	{
		return $this->belongsTo(\App\Models\Proyecto::class, 'proyectos_id');
	}

	public function funcionarios()
	{
		return $this->belongsToMany(\App\Models\Funcionario::class, 'items_funcionarios', 'items_id', 'funcionarios_id')
					->withPivot('id', 'fecha_asignado', 'fecha_desasignado', 'deleted_at')
					->withTimestamps();
	}
}
