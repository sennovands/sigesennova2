<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Funcionario
 *
 * @property int $id
 * @property int $nombre
 * @property int $correo
 * @property int $documento
 * @property int $telefono
 * @property int $tipo_functionarios_id
 * @property int $centros_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Centro $centro
 * @property \App\Models\TipoFunctionario $tipo_functionario
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Funcionario onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereCentrosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereCorreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereDocumento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereTipoFunctionariosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Funcionario whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Funcionario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Funcionario withoutTrashed()
 * @mixin \Eloquent
 */
class Funcionario extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'nombre' => 'string',
		'correo' => 'string',
		'documento' => 'string',
		'telefono' => 'string',
		'tipo_functionarios_id' => 'int',
		'centros_id' => 'int'
	];

	protected $fillable = [
		'nombre',
		'correo',
		'documento',
		'telefono',
		'tipo_functionarios_id',
		'centros_id'
	];

	public function centro()
	{
		return $this->belongsTo(\App\Models\Centro::class, 'centros_id');
	}

	public function tipo_functionario()
	{
		return $this->belongsTo(\App\Models\TipoFunctionario::class, 'tipo_functionarios_id');
	}

	public function items()
	{
		return $this->belongsToMany(\App\Models\Item::class, 'items_funcionarios', 'funcionarios_id', 'items_id')
					->withPivot('id', 'fecha_asignado', 'fecha_desasignado', 'deleted_at')
					->withTimestamps();
	}
}
