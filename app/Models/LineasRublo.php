<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LineasRublo
 *
 * @property int $id
 * @property int $rublos_id
 * @property int $lineas_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Linea $linea
 * @property \App\Models\Rublo $rublo
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LineasRublo onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LineasRublo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LineasRublo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LineasRublo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LineasRublo whereLineasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LineasRublo whereRublosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LineasRublo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LineasRublo withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LineasRublo withoutTrashed()
 * @mixin \Eloquent
 */
class LineasRublo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'rublos_id' => 'int',
		'lineas_id' => 'int'
	];

	protected $fillable = [
		'rublos_id',
		'lineas_id'
	];

	public function linea()
	{
		return $this->belongsTo(\App\Models\Linea::class, 'lineas_id');
	}

	public function rublo()
	{
		return $this->belongsTo(\App\Models\Rublo::class, 'rublos_id');
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class, 'lineas_rublos_id');
	}
}
