<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AreasConocimiento
 *
 * @property int $id
 * @property int $codigo
 * @property string $nucleo_basico
 * @property int $areas_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Area $area
 * @property \Illuminate\Database\Eloquent\Collection $proyectos
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreasConocimiento onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereAreasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereNucleoBasico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AreasConocimiento whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreasConocimiento withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreasConocimiento withoutTrashed()
 * @mixin \Eloquent
 */
class AreasConocimiento extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'areas_conocimiento';

	protected $casts = [
		'codigo' => 'int',
		'areas_id' => 'int'
	];

	protected $fillable = [
		'codigo',
		'nucleo_basico',
		'areas_id'
	];

	public function area()
	{
		return $this->belongsTo(\App\Models\Area::class, 'areas_id');
	}

	public function proyectos()
	{
		return $this->hasMany(\App\Models\Proyecto::class, 'areas_conocimiento_id1');
	}
}
