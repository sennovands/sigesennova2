<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Categoria
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Categoria onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Categoria withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Categoria withoutTrashed()
 * @mixin \Eloquent
 */
class Categoria extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'nombre',
		'descripcion'
	];
}
