<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Regionale
 *
 * @property int $id
 * @property string $nombre
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $codigo
 * @property \Illuminate\Database\Eloquent\Collection $centros
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Regionale onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regionale whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regionale whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regionale whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regionale whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regionale whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regionale whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Regionale withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Regionale withoutTrashed()
 * @mixin \Eloquent
 */
class Regionale extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'nombre',
		'codigo'
	];

	public function centros()
	{
		return $this->hasMany(\App\Models\Centro::class, 'regionales_id');
	}
}
