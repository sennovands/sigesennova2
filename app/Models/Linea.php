<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Linea
 *
 * @property int $id
 * @property string $nombre
 * @property int $valor_maximo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Illuminate\Database\Eloquent\Collection $rublos
 * @property \Illuminate\Database\Eloquent\Collection $proyectos
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Linea onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Linea whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Linea whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Linea whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Linea whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Linea whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Linea whereValorMaximo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Linea withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Linea withoutTrashed()
 * @mixin \Eloquent
 */
class Linea extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'valor_maximo' => 'int'
	];

	protected $fillable = [
		'nombre',
		'valor_maximo'
	];

	public function rublos()
	{
		return $this->belongsToMany(\App\Models\Rublo::class, 'lineas_rublos', 'lineas_id', 'rublos_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

	public function proyectos()
	{
		return $this->hasMany(\App\Models\Proyecto::class, 'lineas_id');
	}
}
