<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DocumentosProyecto
 *
 * @property int $id
 * @property string $nombre
 * @property string $formato
 * @property float $peso
 * @property string $observaciones
 * @property int $proyectos_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Proyecto $proyecto
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentosProyecto onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereFormato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto wherePeso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereProyectosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocumentosProyecto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentosProyecto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentosProyecto withoutTrashed()
 * @mixin \Eloquent
 */
class DocumentosProyecto extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'documentos_proyecto';

	protected $casts = [
		'peso' => 'float',
		'proyectos_id' => 'int'
	];

	protected $fillable = [
		'nombre',
		'formato',
		'peso',
		'observaciones',
		'proyectos_id'
	];

	public function proyecto()
	{
		return $this->belongsTo(\App\Models\Proyecto::class, 'proyectos_id');
	}
}
