<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Proyecto
 *
 * @property int $id
 * @property int $centro_id
 * @property string $subdirector_nombre
 * @property string $subdirector_documento
 * @property string $subdirector_celular
 * @property int $titulo
 * @property string $autores
 * @property int $radico_funcionario_id
 * @property int $lineas_id
 * @property int $areas_conocimiento_id
 * @property int $areas_conocimiento_id1
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Centro $centro
 * @property \App\Models\AreasConocimiento $areas_conocimiento
 * @property \App\Models\Linea $linea
 * @property \Illuminate\Database\Eloquent\Collection $documentos_proyectos
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proyecto onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereAreasConocimientoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereAreasConocimientoId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereAutores($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereCentroId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereLineasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereRadicoFuncionarioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereSubdirectorCelular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereSubdirectorDocumento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereSubdirectorNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proyecto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proyecto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proyecto withoutTrashed()
 * @mixin \Eloquent
 */
class Proyecto extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'centro_id' => 'int',
		'titulo' => 'int',
		'radico_funcionario_id' => 'int',
		'lineas_id' => 'int',
		'areas_conocimiento_id' => 'int',
		'areas_conocimiento_id1' => 'int',
        'año' => 'int'
	];

	protected $fillable = [
		'centro_id',
		'subdirector_nombre',
		'subdirector_documento',
		'subdirector_celular',
		'titulo',
		'autores',
		'radico_funcionario_id',
		'lineas_id',
		'areas_conocimiento_id',
		'areas_conocimiento_id1',
		'estado',
        'año'
	];

	public function centro()
	{
		return $this->belongsTo(\App\Models\Centro::class);
	}

	public function areas_conocimiento()
	{
		return $this->belongsTo(\App\Models\AreasConocimiento::class, 'areas_conocimiento_id1');
	}

	public function linea()
	{
		return $this->belongsTo(\App\Models\Linea::class, 'lineas_id');
	}

	public function documentos_proyectos()
	{
		return $this->hasMany(\App\Models\DocumentosProyecto::class, 'proyectos_id');
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class, 'proyectos_id');
	}
}
