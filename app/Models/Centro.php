<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Centro
 *
 * @property int $id
 * @property string $nombre
 * @property int $regionales_id
 * @property string $subdirector_nombre
 * @property string $subdirector_celular
 * @property string $subdirector_correo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $codigo
 * @property \App\Models\Regionale $regionale
 * @property \Illuminate\Database\Eloquent\Collection $funcionarios
 * @property \Illuminate\Database\Eloquent\Collection $proyectos
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Centro onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereRegionalesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereSubdirectorCelular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereSubdirectorCorreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereSubdirectorNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Centro whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Centro withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Centro withoutTrashed()
 * @mixin \Eloquent
 */
class Centro extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'regionales_id' => 'int'
	];

	protected $fillable = [
		'nombre',
		'regionales_id',
		'subdirector_nombre',
		'subdirector_celular',
		'subdirector_correo',
		'codigo'
	];

	public function regionale()
	{
		return $this->belongsTo(\App\Models\Regionale::class, 'regionales_id');
	}

	public function funcionarios()
	{
		return $this->hasMany(\App\Models\Funcionario::class, 'centros_id');
	}

	public function proyectos()
	{
		return $this->hasMany(\App\Models\Proyecto::class);
	}
}
