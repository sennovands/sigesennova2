<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TipoFunctionario
 *
 * @property int $id
 * @property string $nombre
 * @property int $iscuentadantes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Illuminate\Database\Eloquent\Collection $funcionarios
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoFunctionario onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoFunctionario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoFunctionario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoFunctionario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoFunctionario whereIscuentadantes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoFunctionario whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TipoFunctionario whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoFunctionario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoFunctionario withoutTrashed()
 * @mixin \Eloquent
 */
class TipoFunctionario extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'iscuentadantes' => 'int'
	];

	protected $fillable = [
		'nombre',
		'iscuentadantes'
	];

	public function funcionarios()
	{
		return $this->hasMany(\App\Models\Funcionario::class, 'tipo_functionarios_id');
	}
}
