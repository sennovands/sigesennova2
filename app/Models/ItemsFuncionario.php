<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemsFuncionario
 *
 * @property int $id
 * @property \Carbon\Carbon $fecha_asignado
 * @property \Carbon\Carbon $fecha_desasignado
 * @property int $funcionarios_id
 * @property int $items_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Funcionario $funcionario
 * @property \App\Models\Item $item
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItemsFuncionario onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereFechaAsignado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereFechaDesasignado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereFuncionariosId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereItemsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemsFuncionario whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItemsFuncionario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItemsFuncionario withoutTrashed()
 * @mixin \Eloquent
 */
class ItemsFuncionario extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'funcionarios_id' => 'int',
		'items_id' => 'int'
	];

	protected $dates = [
		'fecha_asignado',
		'fecha_desasignado'
	];

	protected $fillable = [
		'fecha_asignado',
		'fecha_desasignado',
		'funcionarios_id',
		'items_id'
	];

	public function funcionario()
	{
		return $this->belongsTo(\App\Models\Funcionario::class, 'funcionarios_id');
	}

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class, 'items_id');
	}
}
