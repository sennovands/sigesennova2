<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 19:58:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Rublo
 *
 * @property int $id
 * @property string $nombre
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Illuminate\Database\Eloquent\Collection $lineas
 * @package App\Models
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Rublo onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rublo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rublo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rublo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rublo whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rublo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Rublo withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Rublo withoutTrashed()
 * @mixin \Eloquent
 */
class Rublo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'nombre' ,'monto_maximo'
	];

	public function lineas()
	{
		return $this->belongsToMany(\App\Models\Linea::class, 'lineas_rublos', 'rublos_id', 'lineas_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}
}
