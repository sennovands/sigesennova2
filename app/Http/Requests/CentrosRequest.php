<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CentrosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'codigo' => 'int|required|min:100|max:9999|unique:centros,codigo',
                        'nombre' => 'string|required',
                        'regionales_id'=>'int|required|exists:regionales,id',

                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'codigo' => 'int|required|min:100|max:9999|unique:centros,codigo,'.$this->centro,
                        'nombre' => 'string|required',
                        'regionales_id'=>'int|required|exists:regionales,id',
                    ];
                }
            default:break;
        }
    }
}
