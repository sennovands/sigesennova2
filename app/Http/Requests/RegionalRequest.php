<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RegionalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'codigo' => 'int|required|min:1|max:100|unique:regionales,codigo',
                        'nombre' => 'string|required',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'codigo' => 'int|required|min:1|max:100|unique:regionales,codigo,'.$this->regional,
                        'nombre' => 'string|required',
                    ];
                }
            default:break;
        }
    }
}
