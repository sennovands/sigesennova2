<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyectosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $year = date("Y");
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'centro_id' => '',
                        'año' => 'int|min:2014|max:'.$year.'|required',
                        'subdirector_nombre'=>'string|required',
                        'subdirector_documento'=>'string|required',
                        'subdirector_celular'=>'string|required',
                        'titulo'=>'string|required',
                        'autores'=>'string|required',
                        'radico_funcionario_id'=>'int|required|exist:funcionarios,id',
                        'lineas_id'=>'int|required|exist:lineas,id',
                        'areas_conocimiento_id'=> 'int|required|exist: areas_conocimiento,id',
                        'areas_conocimiento_id1'=>'int|required|exist: areas_conocimiento,id',
                        'estado'=>'string|required'
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'nombre' => 'string|required|min:1|max:255|unique:lineas,nombre,'.$this->nombre,
                        'nombre' => 'string|required',
                        'valor_maximo'=>'int|required|min:1|max:999999999999999'
                    ];
                }
            default:break;
        }
    }
}
