<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LineasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'nombre' => 'string|required|min:1|max:255|unique:lineas,nombre',
                        'valor_maximo'=>'int|required|min:1|max:999999999999999'
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'nombre' => 'string|required|min:1|max:255|unique:lineas,nombre,'.$this->nombre,
                        'nombre' => 'string|required',
                        'valor_maximo'=>'int|required|min:1|max:999999999999999'
                    ];
                }
            default:break;
        }
    }
}
