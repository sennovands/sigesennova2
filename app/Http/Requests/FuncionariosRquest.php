<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FuncionariosRquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'documento' => 'int|required|min:1|max:100|unique:funcionarios,documento',
                        'nombre' => 'string|required',
                        'centros_id'=>'int|required|exists:centros,id',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'codigo' => 'int|required|max:9999999999|unique:funcionarios,documento,'.$this->funcionario,
                        'nombre' => 'string|required',
                        'centros_id'=>'int|required|exists:centros,id',
                    ];
                }
            default:break;
        }
    }
}
