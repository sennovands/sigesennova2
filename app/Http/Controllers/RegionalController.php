<?php

namespace App\Http\Controllers;

use App\Http\Requests\CentrosRequest;
use App\Http\Requests\RegionalRequest;
use App\Models\Regionale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegionalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $list = Regionale::withTrashed()->with('centros')->paginate(10);
        return view('regionales.lista', compact([
            'list'
        ]))->with('i', (request()->input('page', 1) - 1) * 5);
        return view('regionales.lista')->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('regionales.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegionalRequest $request)
    {
        //
        Regionale::create($request->all());
        return redirect()->route('regional.index')
            ->with('success','Regional creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $regional = Regionale::findOrFail($id);
        return view('regionales.ver', compact('regional'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $regional = Regionale::findOrFail($id);
        return view('regionales.editar', compact('regional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RegionalRequest $request, $id)
    {
        //
        $regional = Regionale::findOrFail($id);
        $regional->update($request->all());
        return redirect()->route('regional.index')
            ->with('success','Regional actualizada');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $regional = Regionale::findOrFail($id);
        if($regional->delete()){
            return redirect()->route('regional.index')
                ->with('success', "Regional $regional->nomnbre ha sido eliminada");
        }else{
            return redirect()->route('regional.index')
                ->with('error', "Regional $regional->nomnbre no ha podido ser eliminada");
        }
    }

    public function delete($id)
    {
        $regional = Regionale::findOrFail($id);
        return view('regionales.eliminar', compact('regional'));
    }

    public function restore($id){
        $regional = Regionale::withTrashed()->where('id', '=', $id)->first();
        if($regional->restore()){
            return redirect()->route('regional.index')
                ->with('success', "Regional $regional->nomnbre ha sido restaurar");
        }else{
            return redirect()->route('regional.index')
                ->with('error', "Regional $regional->nomnbre no ha podido ser restaurada");
        }
    }


}
