<?php

namespace App\Http\Controllers;

use App\Models\AreasConocimiento;
use App\Models\Centro;
use App\Models\Funcionario;
use App\Models\Linea;
use App\Models\Proyecto;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{

    public $carpeta_views = "proyectos.";
    public $nombre_item = "proyecto";


    public function __construct()
    {
        $this->middleware('auth');
    }



    public function search($tipo){
        $list = array();
        if($tipo=="terminado"){
            $list = Proyecto::withoutTrashed()->where('estado','=', 'terminado')->paginate(10);
        }else if($tipo=="postulado"){
            $list = Proyecto::withoutTrashed()->where('estado','=', 'postulado')->paginate(10);
        }else if($tipo=="ejecución"){
            $list = Proyecto::withoutTrashed()->where('estado','=', 'ejecución')->paginate(10);
        }else if($tipo=="todos"){
            $list = Proyecto::withoutTrashed()->paginate(10);
        }

        return view ('proyectos.lista', compact(['list']));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $funcionarios = Funcionario::withoutTrashed()->get();
        $centros = Centro::withoutTrashed()->get();
        $lineas = Linea::withoutTrashed()->get();
        $areasc = AreasConocimiento::withoutTrashed()->get();

        return view($this->carpeta_views.'crear', compact(['funcionarios', 'centros','lineas','areasc']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $item = Proyecto::findOrFail($id);
        return view($this->carpeta_views . 'eliminar', compact('item'));
    }

    public function restore($id)
    {
        $item = Proyecto::withTrashed()->where('id', '=', $id)->first();
        if ($item->restore()) {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('success', $this->nombre_item . " $item->nomnbre ha sido restaurado");
        } else {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('error', $this->nombre_item . " $item->nomnbre no ha podido ser restaurado");
        }
    }
}
