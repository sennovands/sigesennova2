<?php

namespace App\Http\Controllers;

use App\Models\Centro;
use App\Models\Funcionario;
use App\Models\TipoFunctionario;
use Illuminate\Http\Request;

class FuncionarosController extends Controller
{


    public $carpeta_views = "funcionarios.";
    public $nombre_item = "Funcionario";


    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $list = Funcionario::withTrashed()->paginate(5);
        return view($this->carpeta_views . 'lista', compact([
            'list'
        ]))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipof = TipoFunctionario::withoutTrashed()->get();
        $centros = Centro::all();
        return view('funcionarios.crear',compact(['tipof', 'centros']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Funcionario::create($request->all());

        return redirect()->route('funcionarios.index')
        ->with('success','Funcionario registrado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = Funcionario::findOrFail($id);
        return view('funcionarios.ver', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Funcionario::findOrFail($id);
        $tipof = TipoFunctionario::withoutTrashed()->get();
        $centros = Centro::all();
        return view('funcionarios.editar',compact(['tipof', 'centros', 'item']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $item = Funcionario::findOrFail($id);
        $item->update($request->all());

        return redirect()->route($this->carpeta_views . 'index')
            ->with('success', $this->nombre_item . ' actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $item = Funcionario::findOrFail($id);
        if ($item->delete()) {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('success', $this->nombre_item . " $item->nomnbre ha sido eliminado");
        } else {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('error', $this->nombre_item . " $item->nomnbre no ha podido ser eliminado");
        }
    }

    public function delete($id)
    {
        $item = Funcionario::findOrFail($id);
        return view($this->carpeta_views . 'eliminar', compact('item'));
    }

    public function restore($id)
    {
        $item = Funcionario::withTrashed()->where('id', '=', $id)->first();
        if ($item->restore()) {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('success', $this->nombre_item . " $item->nomnbre ha sido restaurado");
        } else {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('error', $this->nombre_item . " $item->nomnbre no ha podido ser restaurado");
        }
    }
}
