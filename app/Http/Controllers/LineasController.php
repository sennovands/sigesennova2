<?php

namespace App\Http\Controllers;

use App\Http\Requests\LineasRequest;
use App\Models\Linea;
use App\Models\LineasRublo;
use App\Models\Rublo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Diff\Line;


class LineasController extends Controller
{

    public $carpeta_views = "lineas.";
    public $nombre_item = "Linea";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //

        $list = Linea::withTrashed()->paginate(5);
        return view($this->carpeta_views . 'lista', compact([
            'list'
        ]))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $item = Linea::withoutTrashed()->orderBy('nombre', 'ASC')->get();
        $rublos = Rublo::all();
        return view($this->carpeta_views . 'crear', compact(['item', 'rublos']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LineasRequest $request)
    {
        //
        $linea = Linea::create($request->all());
        if (count($request->rublos_asociados) > 0) {
            foreach ($request->rublos_asociados as $rublo) {
                $lineaRublo = new LineasRublo();
                $lineaRublo->rublos_id = $rublo;
                $lineaRublo->lineas_id = $linea->id;
                $lineaRublo->save();
                }
        }


        return redirect()->route($this->carpeta_views . 'index')
            ->with('success', $this->nombre_item . ' creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = Linea::findOrFail($id);
        return view($this->carpeta_views . 'ver', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Linea::findOrFail($id);
        $rublos = Rublo::all();

        return view($this->carpeta_views . 'editar', compact(['item', 'rublos']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(LineasRequest $request, $id)
    {
        //
        $item = Linea::findOrFail($id);
        $item->update($request->all());

        LineasRublo::where('lineas_id','=', $item->id)->forceDelete();
        if (count($request->rublos_asociados) > 0) {
            foreach ($request->rublos_asociados as $rublo) {
                $lineaRublo = new LineasRublo();
                $lineaRublo->rublos_id = $rublo;
                $lineaRublo->lineas_id = $item->id;
                $lineaRublo->save();
            }
        }
        return redirect()->route($this->carpeta_views . 'index')
            ->with('success', $this->nombre_item . ' actualizado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $item = Linea::findOrFail($id);
        if ($item->delete()) {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('success', $this->nombre_item . " $item->nomnbre ha sido eliminado");
        } else {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('error', $this->nombre_item . " $item->nomnbre no ha podido ser eliminado");
        }
    }

    public function delete($id)
    {
        $item = Linea::findOrFail($id);
        return view($this->carpeta_views . 'eliminar', compact('item'));
    }

    public function restore($id)
    {
        $item = Linea::withTrashed()->where('id', '=', $id)->first();
        if ($item->restore()) {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('success', $this->nombre_item . " $item->nomnbre ha sido restaurado");
        } else {
            return redirect()->route($this->carpeta_views . 'index')
                ->with('error', $this->nombre_item . " $item->nomnbre no ha podido ser restaurado");
        }
    }
}
