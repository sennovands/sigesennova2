<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;

class AreasConocimientoController extends Controller
{
    public $carpeta_views = "areasconocimiento.";
    public $nombre_item = "Area";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //

        $list = Area::with('areas_conocimientos')->get();
        return view($this->carpeta_views . 'lista', compact([
            'list'
        ]))->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
