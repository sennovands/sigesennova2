<?php

namespace App\Http\Controllers;

use App\Http\Requests\CentrosRequest;
use Illuminate\Http\Request;
use App\Models\Centro;
use App\Models\Regionale;

class CentrosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //

        $list = Centro::withTrashed()->with('regionale')->paginate(10);
        return view('centros.lista', compact([
            'list'
        ]))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $regionales = Regionale::withoutTrashed()->orderBy('nombre','ASC')->get();
        return view('centros.crear', compact('regionales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CentrosRequest $request)
    {
        //
        Centro::create($request->all());
        return redirect()->route('centros.index')
            ->with('success','Centro de formación creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $centro = Centro::findOrFail($id);
        return view('centros.ver', compact('centro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $regionales = Regionale::withoutTrashed()->orderBy('nombre','ASC')->get();
        $centro = Centro::findOrFail($id);
        return view('centros.editar', compact(['centro','regionales']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CentrosRequest $request, $id)
    {
        //
        $centro = Centro::findOrFail($id);
        $centro->update($request->all());
        return redirect()->route('centros.index')
            ->with('success','Centro actualizado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $regionales = Regionale::withoutTrashed()->orderBy('nombre','ASC')->get();
        $centro = Centro::findOrFail($id);
        if($centro->delete()){
            return redirect()->route('centros.index')
                ->with('success', "Centro $centro->nomnbre ha sido eliminado");
        }else{
            return redirect()->route('centros.index')
                ->with('error', "Centro $centro->nomnbre no ha podido ser eliminado");
        }
    }

    public function delete($id)
    {
        $centro = Centro::findOrFail($id);
        return view('centros.eliminar', compact('centro'));
    }

    public function restore($id){
        $centro = Centro::withTrashed()->where('id', '=', $id)->first();
        if($centro->restore()){
            return redirect()->route('centros.index')
                ->with('success', "Centro $centro->nomnbre ha sido restaurado");
        }else{
            return redirect()->route('centros.index')
                ->with('error', "Centro $centro->nomnbre no ha podido ser restaurado");
        }
    }
}
