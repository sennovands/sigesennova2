<?php

namespace App\Http\Controllers;

use App\Http\Requests\RublosRequest;
use App\Models\Rublo;
use Illuminate\Http\Request;

class RublosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //

        $list = Rublo::withTrashed()->paginate(10);
        return view('rublos.lista', compact([
            'list'
        ]))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $rublo = Rublo::withoutTrashed()->orderBy('nombre', 'ASC')->get();
        return view('rublos.crear', compact('rublo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RublosRequest $request)
    {
        //
        Rublo::create($request->all());
        return redirect()->route('rublos.index')
            ->with('success', 'Rublo creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $rublo = Rublo::findOrFail($id);
        return view('rublos.ver', compact('rublo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rublo = Rublo::findOrFail($id);
        return view('rublos.editar', compact(['rublo']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RublosRequest $request, $id)
    {
        //
        $rublo = Rublo::findOrFail($id);
        $rublo->update($request->all());
        return redirect()->route('rublos.index')
            ->with('success', 'Rublo actualizado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $rublo = Rublo::findOrFail($id);
        if ($rublo->delete()) {
            return redirect()->route('rublos.index')
                ->with('success', "Rublo $rublo->nomnbre ha sido eliminado");
        } else {
            return redirect()->route('centros.index')
                ->with('error', "Rublo $rublo->nomnbre no ha podido ser eliminado");
        }
    }

    public function delete($id)
    {
        $rublo = Rublo::findOrFail($id);
        return view('rublos.eliminar', compact('rublo'));
    }

    public function restore($id)
    {
        $rublo = Rublo::withTrashed()->where('id', '=', $id)->first();
        if ($rublo->restore()) {
            return redirect()->route('rublos.index')
                ->with('success', "Rublo $rublo->nomnbre ha sido restaurado");
        } else {
            return redirect()->route('rublos.index')
                ->with('error', "Rublo $rublo->nomnbre no ha podido ser restaurado");
        }
    }
}
