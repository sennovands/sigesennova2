@extends('layouts.dashboard',['nombre'=>'Nuevo Proyecto'])

@section('content')
    {!! Form::open(['route' => 'lineas.store']) !!}
    <div class="col-md-12">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-8">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Nuevo registro</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <!-- Regular Input -->
                            <div class="form-group">
                                <label for="codigo" class="control-label">Titulo</label>
                                <input type="text" name="titulo" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="Valor maximo" class="control-label">Centro de formación:</label>
                                <select class="select-with-search form-control pmd-select2" name="centro_id">
                                    @foreach($centros as $reg)
                                        <option value="{{$reg->id}}"
                                                @if(old('centro_id')== $reg->id) selected @endif> {{$reg->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="Valor maximo" class="control-label">Autores:</label>
                                <input type="text" name="autores" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="Valor maximo" class="control-label">Radicado por:</label>
                                <select class="select-with-search form-control pmd-select2"
                                        name="radico_funcionario_id">
                                    @foreach($funcionarios as $reg)
                                        <option value="{{$reg->id}}"
                                                @if(old('radico_funcionario_id')== $reg->id) selected @endif> {{$reg->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="codigo" class="control-label">Año:</label>
                                <select class="select-with-search form-control pmd-select2" name="año">
                                    <?php
                                    $year = date("Y");
                                    for ($i = 2014; $i <= $year; $i++){
                                        ?>
                                        <option value="{{$i}}"
                                                @if(old('año')== $i) selected @endif> {{$i}}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="codigo" class="control-label">Linea sennova:</label>
                                <select class="select-with-search form-control pmd-select2" name="lineas_id">
                                    @foreach($lineas as $reg)
                                        <option value="{{$reg->id}}"
                                                @if(old('lineas_id')== $reg->id) selected @endif> {{$reg->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Area de conocimiento 1:</label>
                                <select class="select-with-search form-control pmd-select2"
                                        name="areas_conocimiento_id">
                                    @foreach($areasc as $reg)
                                        <option value="{{$reg->id}}"
                                                @if(old('areas_conocimiento_id')== $reg->id) selected @endif>
                                            [{{$reg->codigo}}] {{$reg->nucleo_basico}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Area de conocimiento 2:</label>
                                <select class="select-with-search form-control pmd-select2"
                                        name="areas_conocimiento_id1">
                                    @foreach($areasc as $reg)
                                        <option value="{{$reg->id}}"
                                                @if(old('areas_conocimiento_id1')== $reg->id) selected @endif>
                                            [{{$reg->codigo}}] {{$reg->nucleo_basico}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <center>
                                    <a class="btn btn-primary" href="{{route('lineas.index')}}">CANCELAR</a>
                                    <button class="btn btn-success" type="submit">GUARDAR</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->

        </div>
    </div><!--end Text fields code, example -->
    {!! Form::close() !!}
@endsection


@section('scripts')
    <script>
        $(".select-with-search").select2({
            theme: "bootstrap"
        });
        <!-- Select Multiple Tags -->
        /*
        $(".select-tags").select2({
            tags: false,
            theme: "bootstrap",
        });*/
    </script>
@endsection