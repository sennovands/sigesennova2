 @extends('layouts.dashboard',['nombre'=>'Lineas'])

@section('content')
    <div class="col-md-12">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Detalle de la Regional</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Nombre:</b><br>
                                    {{$item->nombre}}
                                </div>
                                <div class="col-md-9">
                                    <b>Valor maximo:</b><br>
                                    {{number_format($item->valor_maximo)}}
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-12">
                                    <b>Rublos asociados a la linea:</b><br>
                                    @foreach($item->rublos as $aux)
                                        <li>{{$aux->nombre}}</li>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-3">
                                    <b>Estado:</b><br>
                                    {!! $item->deleted_at == null ? '<span class="text-success">ACTIVO</span>' : '<span class="text-danger">ELIMINADO</span>' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->

        </div>
    </div><!--end Text fields code, example -->
@endsection