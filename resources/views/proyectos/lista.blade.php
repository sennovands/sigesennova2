@extends('layouts.dashboard',['nombre'=>'Listado de proyectos'])

@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px">
            <div class="btn-group">
                <a href="{{route('proyectos.create')}}" class="btn pmd-btn-raised pmd-ripple-effect btn-info"
                   type="button">Nuevo proyecto</a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- table card code and example -->
        <div class="col-md-12">
            <div class="component-box">

                <!-- Card table example -->
                <div class="pmd-card pmd-z-depth pmd-card-custom-view">
                    <div class="pmd-table-card">
                        <table class="table pmd-table">
                            <thead>
                            <tr class="tr-bold">
                                <th style="width: 30%">Titulo</th>
                                <th style="width: 20%">Linea</th>
                                <th style="width: 40%">Autores</th>
                                <th style="width: 10%">Fecha</th>
                                <th style="width: 40px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($list)==0)
                                <tr>
                                    <td colspan="5">
                                        <center><br><br><br><b>NO HAY REGISTROS QUE VISUALIZAR</b><br><br><br></center>
                                    </td>
                                </tr>
                            @endif
                            @foreach($list as $row)
                                <tr @if( $row->deleted_at != null ) class="text-danger" @endif >
                                    <td data-title="Titulo">{{{$row->titulo}}}</td>
                                    <td data-title="Linea">{{$row->linea->nombre}}</td>
                                    <td data-title="Linea">{{$row->autores}}</td>
                                    <td data-title="Fecha">{{$row->updated_at}}</td>
                                    <td data-title="">
                                        <span class="dropdown pmd-dropdown clearfix">
												<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary"
                                                        type="button" id="dropdownMenuBottomRight"
                                                        data-toggle="dropdown" aria-expanded="true"><i
                                                            class="material-icons pmd-sm">more_vert</i></button>
												<ul aria-labelledby="dropdownMenuDivider" role="menu"
                                                    class="dropdown-menu dropdown-menu-right bg-white">
													<li role="presentation"><a
                                                                href="{{route('lineas.show',[$row->id])}}"
                                                                tabindex="-1"
                                                                role="menuitem">Ver</a></li>
													<li role="presentation"><a
                                                                href="{{route('lineas.edit',$row->id)}}" tabindex="-1"
                                                                role="menuitem">Editar</a></li>
                                                    {{--<li class="divider" role="presentation"></li>
                                                    @if( $row->deleted_at != null )
                                                        <li role="presentation"><a
                                                                    href="{{route('lineas.restore',[$row->id])}}"
                                                                    tabindex="-1"
                                                                    role="menuitem"><span
                                                                        class="text-info">Restaurar</span></a></li>
                                                    @else
                                                        <li role="presentation"><a
                                                                    href="{{route('lineas.delete',[$row->id])}}"
                                                                    tabindex="-1"
                                                                    role="menuitem"><span
                                                                        class="text-danger">Eliminar</span></a></li>
                                                    @endif--}}
												</ul>
											</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> <!-- Card table example end -->

            </div>
        </div> <!-- Card table code and example end -->

    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <center>
                {{$list->links()}}
            </center>
        </div>
    </div>
@endsection

@section('extras')

@endsection

