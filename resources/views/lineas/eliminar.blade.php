@extends('layouts.dashboard',['nombre'=>'Lineas'])

@section('content')
    {!! Form::model($item, ['method' => 'DELETE','route' => ['lineas.destroy', $item->id]]) !!}

    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Eliminar regitsro</h2>
                            <span class="pmd-card-subtitle-text">
                                Esta a punto de eliminar el registro con la siguiente información:
                            </span>
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <!-- Regular Input -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Nombre</label><br>
                                        <b>{{$item->nombre}}</b>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Valor maximo</label><br>
                                        <b>${{number_format($item->valor_maximo)}}</b>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Rublos asociados</label>
                                        <b>
                                            @foreach($item->rublos as $aux)
                                                <li>{{$aux->nombre}}</li>
                                            @endforeach
                                        </b>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <br>
                                        <b class="text-danger">¿Confirma la eliminación de dicho registro?</b>
                                        <br>
                                        <br>
                                    </center>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>
                                            <a class="btn btn-primary" href="{{route('lineas.index')}}">NO,
                                                CANCELAR</a>
                                            <button class="btn btn-success" type="submit">SI, ELIMINAR</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Text fields example -->
            </div>
        </div><!--end Text fields code, example -->
    </div>
    {!! Form::close() !!}
@endsection