@extends('layouts.dashboard',['nombre'=>'Lineas'])

@section('content')
    {!! Form::model($item, ['method' => 'PATCH','route' => ['lineas.update', $item->id]]) !!}

    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Nuevo registro</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <!-- Regular Input -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Nombre</label>
                                        <input type="text" name="nombre" class="form-control"
                                               value="{{empty(old('nombre')) ? $item->nombre : old('nombre')}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Valor maximo" class="control-label">Valor maximo</label>
                                        <input type="text" name="valor_maximo" class="form-control" value="{{empty(old('valor_maximo')) ? $item->valor_maximo : old('valor_maximo')}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="codigo" class="control-label">Rublos asociados a la linea</label>
                                    <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                        <select class="form-control select-tags pmd-select2-tags"
                                                name="rublos_asociados[]" multiple>
                                            @foreach($rublos as $reg)

                                                <option value="{{$reg->id}}"
                                                        @php
                                                            foreach ($item->rublos as $r) if($r->id == $reg->id){
                                                             echo 'selected';break;
                                                             }
                                                        @endphp
                                                >{{$reg->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>
                                            <a class="btn btn-primary" href="{{route('lineas.index')}}">CANCELAR</a>
                                            <button class="btn btn-success" type="submit">GUARDAR</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Text fields example -->
            </div>
        </div><!--end Text fields code, example -->
    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        /*$(".select-with-search").select2({
            theme: "bootstrap"
        });*/
        <!-- Select Multiple Tags -->
        $(".select-tags").select2({
            tags: false,
            theme: "bootstrap",
        });
    </script>
@endsection