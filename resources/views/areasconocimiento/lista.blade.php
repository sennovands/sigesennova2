@extends('layouts.dashboard',['nombre'=>'Áreas de conocimiento'])

@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px">
            <div class="btn-group">
                <a href="{{route('lineas.create')}}" class="btn pmd-btn-raised pmd-ripple-effect btn-info"
                   type="button">Nuevo registro</a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- table card code and example -->
        <div class="col-md-10">
            <div class="component-box">

                <!-- Card table example -->
                <div class="pmd-card pmd-z-depth pmd-card-custom-view">
                    <div class="pmd-table-card">
                        <table class="table pmd-table table-bordered">
                            <thead>
                            <tr class="tr-bold">
                                <th style="width: 100px">CODIGO</th>
                                <th style="width: 30%">AREA</th>
                                <th>NUCLEO BASICO DE CONOCIMIENTO</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($list)==0)
                                <tr>
                                    <td colspan="5">
                                        <center><br><br><br><b>NO HAY REGISTROS QUE VISUALIZAR</b><br><br><br></center>
                                    </td>
                                </tr>
                            @endif
                            @foreach($list as $row)
                                <tr style="font-weight: bolder">
                                    <td data-title="CODIGO">{{$row->codigo}}</td>
                                    <td data-title="AREA" colspan="2">{{$row->nombre}}</td>
                                </tr>
                                @foreach($row->areas_conocimientos as $areac)
                                    <tr>
                                        <td data-title="CODIGO">{{$areac->codigo}}</td>
                                        <td data-title="AREA: &emsp;{{$row->nombre}}">&nbsp;</td>
                                        <td data-title="NUCLEO BASICO DE CONOCIMIENTO" colspan="2">{{$areac->nucleo_basico}}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> <!-- Card table example end -->

            </div>
        </div> <!-- Card table code and example end -->

    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <center>

            </center>
        </div>
    </div>
@endsection

@section('extras')

@endsection

