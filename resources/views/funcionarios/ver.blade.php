@extends('layouts.dashboard',['nombre'=>'Funcionarios'])

@section('content')
    <div class="col-md-12">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Detalle del funcionario</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <div class="form-group">
                                <label for="codigo" class="control-label">Nombre</label>
                                <b><br>{{$item->nombre}}</b>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">No. de documento</label>
                                <b><br>{{$item->documento}}</b>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Correo electrónico</label>
                                <b><br>{{$item->correo}}</b>
                            </div>
                            <div class="form-group">
                                <label for="Valor maximo" class="control-label">Teléfono</label>
                                <b><br>{{$item->telefono}}</b>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Centro de formación asociado</label>
                                <b><br>{{$item->centro->nombre}}</b>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Tipo de funcionario</label>
                                <b><br>{{$item->tipo_functionario->nombre}}</b>
                            </div>
                        </div>
                    </div>
                </div><!-- end Text fields example -->

            </div>
        </div><!--end Text fields code, example -->
@endsection