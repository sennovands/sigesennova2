@extends('layouts.dashboard',['nombre'=>'Crear Funcionario'])

@section('content')
    {!! Form::open(['route' => 'funcionarios.store']) !!}
    <div class="col-md-12">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-8">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Nuevo registro</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <!-- Regular Input -->
                            <div class="form-group">
                                <label for="codigo" class="control-label">Nombre</label>
                                <input type="text" name="nombre" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">No. de documento</label>
                                <input type="text" name="documento" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Correo electrónico</label>
                                <input type="text" name="correo" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="Valor maximo" class="control-label">Teléfono</label>
                                <input type="text" name="telefono" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Centro de formación asociado</label>
                                <select class="select-with-search form-control pmd-select2" name="centros_id">
                                    @foreach($centros as $reg)
                                        <option value="{{$reg->id}}" @if(old('centros_id')== $reg->id) selected @endif>{{$reg->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Tipo de funcionario</label>
                                <select class="select-with-search form-control pmd-select2" name="tipo_functionarios_id">
                                    @foreach($tipof as $reg)
                                        <option value="{{$reg->id}}" @if(old('tipo_functionarios_id')== $reg->id) selected @endif>{{$reg->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <center>
                                    <a class="btn btn-primary" href="{{route('funcionarios.index')}}">CANCELAR</a>
                                    <button class="btn btn-success" type="submit">GUARDAR</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->

        </div>
    </div><!--end Text fields code, example -->
    {!! Form::close() !!}
@endsection


@section('scripts')
    <script>
        $(".select-with-search").select2({
            theme: "bootstrap"
        });
    </script>
@endsection