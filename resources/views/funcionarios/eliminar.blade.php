@extends('layouts.dashboard',['nombre'=>'Funcionario'])

@section('content')
    {!! Form::model($item, ['method' => 'DELETE','route' => ['funcionarios.destroy', $item->id]]) !!}

    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Eliminar regitsro</h2>
                            <span class="pmd-card-subtitle-text">
                                Esta a punto de eliminar el registro con la siguiente información:
                            </span>
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <!-- Regular Input -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Nombre</label><br>
                                        <b>{{$item->nombre}}</b>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Documento</label><br>
                                        <b>{{number_format($item->documento)}}</b>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Centro de formación</label><br>
                                        <b>{{$item->centro->nombre}}</b>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <center>
                                        <br>
                                        <b class="text-danger">¿Confirma la eliminación de dicho registro?</b>
                                        <br>
                                        <br>
                                    </center>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>
                                            <a class="btn btn-primary" href="{{route('funcionarios.index')}}">NO,
                                                CANCELAR</a>
                                            <button class="btn btn-success" type="submit">SI, ELIMINAR</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Text fields example -->
            </div>
        </div><!--end Text fields code, example -->
    </div>
    {!! Form::close() !!}
@endsection