<div class="static-top col-md-4" id="alert">
    <div class="alert alert-success" role="alert">
        <button class="btn-link alert-hide" type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times"></i> </span>
        </button>
        <strong>Operación realizada!</strong>
        <span>{{$msg}}</span>
    </div>
</div>