<div class="static-top col-md-4" id="alert">
    <div class="alert alert-danger" role="alert">
        <button class="btn-link alert-hide" type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times"></i> </span>
        </button>
        <strong>Upps! Hay algunos problemas en el formulario</strong>
        <span>{{isset($msg) ? $msg : ""}}</span>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>