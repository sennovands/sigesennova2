@php
    $bgcolor = '#214ebf';
    $name = '';
    if(isset($nombre)){
        $name = $nombre;
        if(strpos(strtolower($nombre), 'regional') !== false){
            $name = 'Regionales';
        }
        else if(strpos(strtolower($nombre), 'centro') !== false){
            $name = 'Centros';
        }
        else if(strpos(strtolower($nombre), 'rublo') !== false){
            $name = 'Rublos';
        }
        else if(strpos(strtolower($nombre), 'regional') !== false){
            $name = 'Regionales';
        }else if(strpos(strtolower($nombre), 'funcionario') !== false){
            $name = 'funcionarios';

        }
        else if(strpos(strtolower($nombre), 'proyecto') !== false){
            $name = 'proyectos';

        }
        else if(strpos(strtolower($nombre), 'lineas') !== false){
            $name = 'Lineas';
        } else if($nombre== "Áreas de conocimiento"){
            $name = 'areasc';
        }
    }
@endphp

<li @if($name=='Inicio') class="fe-selected" @endif >
    <a class="pmd-ripple-effect" href="{{url('/home')}}">
        <i class="media-left media-middle"><i class="fas fa-bars"></i></i>
        <span class="media-body">Inicio</span>
    </a>
</li>

<li class="dropdown pmd-dropdown @if($name == 'Regionales' || $name=="Centros" || $name=='Rublos' || $name=='Lineas' || $name=='areasc') open @endif">
    <a aria-expanded="false" data-toggle="dropdown" class="btn-user dropdown-toggle media" data-sidebar="true"
       href="javascript:void(0);">
        <i class="media-left media-middle"><i class="fa fa-cogs"></i></i>
        <span class="media-body">Configuración</span>
        <div class="media-right media-bottom"><i class="dic-more-vert dic"></i></div>
    </a>
    <ul class="dropdown-menu" style="padding: 0">
        <li @if($name=='Regionales') class="fe-selected" @endif ><a href="{{route('regional.index')}}">Regionales</a>
        </li>
        <li @if($name=='Centros') class="fe-selected" @endif ><a href="{{route('centros.index')}}">Centros</a></li>
        <li @if($name=='Rublos') class="fe-selected" @endif><a href="{{route('rublos.index')}}">Rublos</a></li>
        <li @if($name=='Lineas') class="fe-selected" @endif><a href="{{route('lineas.index')}}">Lineas</a></li>
        <li @if($name=='areasc') class="fe-selected" @endif><a href="{{route('areas.index')}}">Áreas de conocimiento</a>
        </li>
        <!--<li><a href="badge.html">Categorias</a></li>-->
    </ul>
</li>

<li @if($name=='funcionarios') class="fe-selected" @endif >
    <a class="pmd-ripple-effect" href="{{route('funcionarios.index')}}">
        <i class="media-left media-middle"><i class="fas fa-bars"></i></i>
        <span class="media-body">Funcionarios</span>
    </a>
</li>

<li class="dropdown pmd-dropdown @if(strpos(strtolower($nombre), 'proyecto') !== false) open @endif">
    <a aria-expanded="false" data-toggle="dropdown" class="btn-user dropdown-toggle media" data-sidebar="true"
       href="javascript:void(0);">
        <i class="media-left media-middle"><i class="fas fa-file-archive"></i></i>
        <span class="media-body">Proyectos</span>
        <div class="media-right media-bottom"><i class="dic-more-vert dic"></i></div>
    </a>
    <ul class="dropdown-menu" style="padding: 0">
        <li @if(strtolower($nombre)== "nuevo proyecto") class="fe-selected" @endif ><a href="{{route('proyectos.create')}}">Nuevo
                proyecto</a>
        </li>
        <li @if(strtolower($nombre)== "Listado de proyectos") class="fe-selected" @endif ><a href="{{route('proyectos.search',['tipo'=>'todos'])}}">Listado de
                proyectos</a>
        </li>
        <!--<li><a href="badge.html">Categorias</a></li>-->
    </ul>
</li>

<li class="dropdown pmd-dropdown @if(strpos(strtolower($nombre), 'proyectos') !== false) open @endif">
    <a aria-expanded="false" data-toggle="dropdown" class="btn-user dropdown-toggle media" data-sidebar="true"
       href="javascript:void(0);">
        <i class="media-left media-middle"><i class="fas fa-file-archive"></i></i>
        <span class="media-body">Inventario</span>
        <div class="media-right media-bottom"><i class="dic-more-vert dic"></i></div>
    </a>
    <ul class="dropdown-menu" style="padding: 0">
        <li @if($name=='Regionales') class="fe-selected" @endif ><a href="{{route('regional.index')}}">Nuevo
                elemento</a>
        </li>
        <li @if($name=='areasc') class="fe-selected" @endif><a href="{{route('areas.index')}}">lista de elementos</a>
        </li>
        <li @if($name=='Rublos') class="fe-selected" @endif><a href="{{route('rublos.index')}}">Traslado</a></li>
        </li>
        <!--<li><a href="badge.html">Categorias</a></li>-->
    </ul>
</li>