<div tabindex="-1" class="modal fade" id="alert-dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="md-delete-text"></div>
            <div class="pmd-modal-action text-right">
                <button data-dismiss="modal" class="btn pmd-ripple-effect btn-primary pmd-btn-flat" type="button">
                    Save changes
                </button>
                <button data-dismiss="modal" class="btn pmd-ripple-effect btn-default pmd-btn-flat" type="button">
                    Discard
                </button>
            </div>
        </div>
    </div>
</div>