@extends('layouts.dashboard',['nombre'=>'Regionales'])

@section('content')
    {!! Form::open(['route' => 'regional.store']) !!}
    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Nuevo registro</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <!-- Regular Input -->
                            <div class="form-group">
                                <label for="codigo" class="control-label">Código</label>
                                <input type="text" name="codigo" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Nombre</label>
                                <input type="text" name="nombre" class="form-control">
                            </div>
                            <div class="form-group">
                                <center>
                                    <a class="btn btn-primary" href="{{route('regional.index')}}">CANCELAR</a>
                                    <button class="btn btn-success" type="submit">GUARDAR</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->

        </div>
    </div><!--end Text fields code, example -->
    {!! Form::close() !!}
@endsection