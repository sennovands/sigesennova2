@extends('layouts.dashboard',['nombre'=>'Regionales'])

@section('content')
    {!! Form::model($regional, ['method' => 'PATCH','route' => ['regional.update', $regional->id]]) !!}

    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Nuevo registro</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <!-- Regular Input -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Código</label>
                                        <input type="text" name="codigo" class="form-control"
                                               value="{{empty(old('codigo')) ? $regional->codigo : old('codigo')}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Nombre</label>
                                        <input type="text" name="nombre" class="form-control"
                                               value="{{empty(old('nombre')) ? $regional->nombre : old('nombre')}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>
                                            <a class="btn btn-primary" href="{{route('regional.index')}}">CANCELAR</a>
                                            <button class="btn btn-success" type="submit">GUARDAR</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Text fields example -->
            </div>
        </div><!--end Text fields code, example -->
    </div>
    {!! Form::close() !!}
@endsection