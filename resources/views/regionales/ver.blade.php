@extends('layouts.dashboard',['nombre'=>'Regionales'])

@section('content')
    <div class="col-md-12">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Detalle de la Regional</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Código:</b><br>
                                    {{$regional->codigo}}
                                </div>
                                <div class="col-md-9">
                                    <b>Nombre:</b><br>
                                    {{$regional->nombre}}
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Estado:</b><br>
                                    {!! $regional->deleted_at == null ? '<span class="text-success">ACTIVO</span>' : '<span class="text-danger">ELIMINADO</span>' !!}
                                </div>
                                <div class="col-md-3">
                                    <b>Fecha de creación:</b><br>
                                    {{$regional->created_at}}
                                </div>
                                <div class="col-md-3">
                                    <b>Fecha ultima modificación:</b><br>
                                    {{$regional->updated_at}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->

        </div>
    </div><!--end Text fields code, example -->
@endsection