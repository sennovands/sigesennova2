@extends('layouts.dashboard',['nombre'=>'Regionales'])

@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px">
            <div class="btn-group">
                <a href="{{route('regional.create')}}" class="btn pmd-btn-raised pmd-ripple-effect btn-info"
                   type="button">Nuevo registro</a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- table card code and example -->
        <div class="col-md-12">
            <div class="component-box">

                <!-- Card table example -->
                <div class="pmd-card pmd-z-depth pmd-card-custom-view">
                    <div class="pmd-table-card">
                        <table class="table pmd-table">
                            <thead>
                            <tr class="tr-bold">
                                <th style="width: 15%">Código</th>
                                <th>Nombre</th>
                                <th style="width: 10%">No. Centros</th>
                                <th style="width: 10%">Fecha Mod.</th>
                                <th style="width: 40px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($list)==0)
                                <tr>
                                    <td colspan="5">
                                        <center><br><br><br><b>NO HAY REGISTROS QUE VISUALIZAR</b><br><br><br></center>
                                    </td>
                                </tr>
                            @endif
                            @foreach($list as $row)
                                <tr @if( $row->deleted_at != null ) class="text-danger" @endif >
                                    <td data-title="Código">{{$row->codigo}}</td>
                                    <td data-title="Nombre">{{{$row->nombre}}}</td>
                                    <td data-title="No. de centros">{{count($row->centros)}}</td>
                                    <td data-title="Fecha Mod.">{{$row->updated_at}}</td>
                                    <td data-title="">
                                        <span class="dropdown pmd-dropdown clearfix">
												<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary"
                                                        type="button" id="dropdownMenuBottomRight"
                                                        data-toggle="dropdown" aria-expanded="true"><i
                                                            class="material-icons pmd-sm">more_vert</i></button>
												<ul aria-labelledby="dropdownMenuDivider" role="menu"
                                                    class="dropdown-menu dropdown-menu-right bg-white">
													<li role="presentation"><a href="{{route('regional.show',[$row->id])}}" tabindex="-1"
                                                                               role="menuitem">Ver</a></li>
													<li role="presentation"><a
                                                                href="{{route('regional.edit',$row->id)}}" tabindex="-1"
                                                                role="menuitem">Editar</a></li>
													<li class="divider" role="presentation"></li>
                                                    @if( $row->deleted_at != null )
                                                    <li role="presentation"><a
                                                                href="{{route('regional.restore',[$row->id])}}"
                                                                tabindex="-1"
                                                                role="menuitem"><span
                                                                    class="text-info">Restaurar</span></a></li>
                                                    @else
                                                        <li role="presentation"><a
                                                                    href="{{route('regional.delete',[$row->id])}}"
                                                                    tabindex="-1"
                                                                    role="menuitem"><span
                                                                        class="text-danger">Eliminar</span></a></li>
                                                    @endif
												</ul>
											</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> <!-- Card table example end -->

            </div>
        </div> <!-- Card table code and example end -->

    </div>


    <div class="row">
        <div class="col-md-12">
            <br>
            <center>
                {{$list->links()}}
            </center>
        </div>
    </div>


@endsection

@section('extras')

@endsection

