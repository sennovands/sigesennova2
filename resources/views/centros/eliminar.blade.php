@extends('layouts.dashboard',['nombre'=>'Centros'])

@section('content')
    {!! Form::model($centro, ['method' => 'DELETE','route' => ['centros.destroy', $centro->id]]) !!}

    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Eliminar regitsro</h2>
                            <span class="pmd-card-subtitle-text">
                                Esta a punto de eliminar el registro con la siguiente información:
                            </span>
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <!-- Regular Input -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Código</label>
                                        <input type="text" name="codigo" class="form-control"
                                               value="{{empty(old('codigo')) ? $centro->codigo : old('codigo')}}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Nombre</label>
                                        <input type="text" name="nombre" class="form-control"
                                               value="{{empty(old('nombre')) ? $centro->nombre : old('nombre')}}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Regional a la que pertenece</label>
                                        <input type="text" name="nombre" class="form-control"
                                               value="[ {{$centro->regionale->codigo}} ] {{$centro->regionale->nombre}}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <br>
                                        <b class="text-danger">¿Confirma la eliminación de dicho registro?</b>
                                        <br>
                                        <br>
                                    </center>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>
                                            <a class="btn btn-primary" href="{{route('centros.index')}}">NO,
                                                CANCELAR</a>
                                            <button class="btn btn-success" type="submit">SI, ELIMINAR</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Text fields example -->
            </div>
        </div><!--end Text fields code, example -->
    </div>
    {!! Form::close() !!}
@endsection