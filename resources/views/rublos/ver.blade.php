@extends('layouts.dashboard',['nombre'=>'Rublos'])

@section('content')
    <div class="col-md-12">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Detalle de la Regional</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Nombre</label><br>
                                        <b>{{empty(old('nombre')) ? $rublo->nombre : old('nombre')}}</b>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="codigo" class="control-label">Monto maximo</label><br>
                                        <b>{{ number_format(empty(old('monto_maximo')) ? $rublo->monto_maximo : old('monto_maximo'))}}</b>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Estado:</b><br>
                                    {!! $centro->deleted_at == null ? '<span class="text-success">ACTIVO</span>' : '<span class="text-danger">ELIMINADO</span>' !!}
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->

        </div>
    </div><!--end Text fields code, example -->
@endsection