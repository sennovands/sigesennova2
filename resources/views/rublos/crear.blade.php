@extends('layouts.dashboard',['nombre'=>'Rublos'])

@section('content')
    {!! Form::open(['route' => 'rublos.store']) !!}
    <div class="col-md-6">
        <div class="component-box">

            <!-- Text fields example -->
            <div class="row">
                <div class="col-md-12">
                    <div class="pmd-card pmd-z-depth pmd-card-custom-form">
                        <div class="pmd-card-title">
                            <h2 class="pmd-card-title-text">Nuevo registro</h2>
                            <!--<span class="pmd-card-subtitle-text">Secondary text</span>-->
                        </div>
                        <div class="pmd-card-body">
                            <!-- Regular Input -->
                            <div class="form-group">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input type="text" name="nombre" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="codigo" class="control-label">Monto maximo</label>
                                <input type="number" name="monto_maximo" class="form-control">
                            </div>
                            <div class="form-group">
                                <center>
                                    <a class="btn btn-primary" href="{{route('rublos.index')}}">CANCELAR</a>
                                    <button class="btn btn-success" type="submit">GUARDAR</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end Text fields example -->
        </div>
    </div><!--end Text fields code, example -->
    {!! Form::close() !!}
@endsection


@section('scripts')
    <script>
        $(".select-with-search").select2({
            theme: "bootstrap"
        });
    </script>
@endsection