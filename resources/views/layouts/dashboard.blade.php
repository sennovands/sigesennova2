<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Blank Page | Propeller - Admin Dashboard">
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{env('APP_NAME')}}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('themes/images/favicon.ico')}}">


    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Propeller css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/propeller.min.css')}}">
    <!-- DataTables css-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
    <!-- Propeller dataTables css-->
    <link rel="stylesheet" type="text/css" href="{{asset('components/data-table/css/pmd-datatable.css')}}">
    <!-- Select2 css-->
    <link rel="stylesheet" type="text/css" href="{{asset('components/select2/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('components/select2/css/select2-bootstrap.css')}}" />
    <!--<link rel="stylesheet" type="text/css" href="{{asset('components/select2/css/pmd-select2.css')}}" />-->


    <!-- Propeller theme css-->
    <link rel="stylesheet" type="text/css" href="{{asset('themes/css/propeller-theme.css')}}"/>
    <!-- Propeller admin theme css-->
    <link rel="stylesheet" type="text/css" href="{{asset('themes/css/propeller-admin.css')}}">
    <!-- font awensome -->
    <script defer src="{{asset('fontawensome/js/fontawesome-all.js')}}"></script>
    <!-- app css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">




</head>

<body>


<!-- MENSAJES DE ALERTA Y ERROR -->
@if ($msg = Session::get('success'))
    @include('includes.alert-success',['msg'=>$msg])
@endif
@if (count($errors)>0)
    @include('includes.alert-error',['msg'=>$msg,'errors'=>$errors])
@endif
<!-- FIN MENSAJES DE ALERTA Y ERROR -->

<!-- Header Starts -->
<!--Start Nav bar -->
<nav class="navbar navbar-inverse navbar-fixed-top pmd-navbar pmd-z-depth">
    <div class="container-fluid">
        <div class="pmd-navbar-right-icon pull-right navigation">
            <!-- Notifications -->
            <div class="dropdown notification icons pmd-dropdown">

                <a href="javascript:void(0)" title="Notification" class="dropdown-toggle pmd-ripple-effect"
                   data-toggle="dropdown" role="button" aria-expanded="true">
                    <i class="fa fa-bell white" style="font-size: 1.5em"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right pmd-card pmd-card-default pmd-z-depth" role="menu">
                    <!-- Card header -->
                    <div class="pmd-card-title">
                        <div class="media-body media-middle">
                            <a href="notifications.html" class="pull-right">3 new notifications</a>
                            <h3 class="pmd-card-title-text">Notifications</h3>
                        </div>
                    </div>

                    <!-- Notifications list -->
                    <ul class="list-group pmd-list-avatar pmd-card-list">
                        <li class="list-group-item hidden">
                            <p class="notification-blank">
                                <span class="dic dic-notifications-none"></span>
                                <span>You don´t have any notifications</span>
                            </p>
                        </li>
                        <li class="list-group-item unread">
                            <a href="javascript:void(0)">
                                <div class="media-left">
									<span class="avatar-list-img40x40">
										<img alt="40x40" data-src="holder.js/40x40" class="img-responsive"
                                             src="{{asset('themes/images/profile-1.png')}}" data-holder-rendered="true">
									</span>
                                </div>
                                <div class="media-body">
                                    <span class="list-group-item-heading"><span>Prathit</span> posted a new challanegs</span>
                                    <span class="list-group-item-text">5 Minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="javascript:void(0)">
                                <div class="media-left">
									<span class="avatar-list-img40x40">
										<img alt="40x40" data-src="holder.js/40x40" class="img-responsive"
                                             src="{{url('themes/images/profile-2.png')}}" data-holder-rendered="true">
									</span>
                                </div>
                                <div class="media-body">
                                    <span class="list-group-item-heading"><span>Keel</span> Cloned 2 challenges.</span>
                                    <span class="list-group-item-text">15 Minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item unread">
                            <a href="javascript:void(0)">
                                <div class="media-left">
									<span class="avatar-list-img40x40">
										<img alt="40x40" data-src="holder.js/40x40" class="img-responsive"
                                             src="{{url('themes/images/profile-3.png')}}" data-holder-rendered="true">
									</span>
                                </div>

                                <div class="media-body">
                                    <span class="list-group-item-heading"><span>John</span> posted new collection.</span>
                                    <span class="list-group-item-text">25 Minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item unread">
                            <a href="javascript:void(0)">
                                <div class="media-left">
									<span class="avatar-list-img40x40">
										<img alt="40x40" data-src="holder.js/40x40" class="img-responsive"
                                             src="{{url('themes/images/profile-4.png')}}" data-holder-rendered="true">
									</span>
                                </div>
                                <div class="media-body">
                                    <span class="list-group-item-heading"><span>Valerii</span> Shared 5 collection.</span>
                                    <span class="list-group-item-text">30 Minutes ago</span>
                                </div>
                            </a>
                        </li>
                    </ul><!-- End notifications list -->

                </div>


            </div> <!-- End notifications -->
        </div>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a href="javascript:void(0);" data-target="basicSidebar" data-placement="left" data-position="slidepush"
               is-open="true" is-open-width="1200"
               class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect pull-left margin-r8 pmd-sidebar-toggle"><i
                        class="material-icons md-light">menu</i></a>
            <a href="index.html" class="navbar-brand">
                SENNOVA
            </a>
        </div>
    </div>

</nav><!--End Nav bar -->
<!-- Header Ends -->

<!-- Sidebar Starts -->
<div class="pmd-sidebar-overlay"></div>

<!-- Left sidebar -->
<aside id="basicSidebar"
       class="pmd-sidebar  sidebar-default pmd-sidebar-slide-push pmd-sidebar-left pmd-sidebar-open bg-fill-darkblue sidebar-with-icons"
       role="navigation">
    <ul class="nav pmd-sidebar-nav">

        <!-- User info -->
        <li class="dropdown pmd-dropdown pmd-user-info visible-xs visible-md visible-sm visible-lg">
            <a aria-expanded="false" data-toggle="dropdown" class="btn-user dropdown-toggle media" data-sidebar="true"
               aria-expandedhref="javascript:void(0);">
                <div class="media-left">
                    <img src="{{url('themes/images/user-icon.png')}}" alt="New User">
                </div>
                <div class="media-body media-middle">{{explode(" ",Auth::user()->name)[0]}}</div>
                <div class="media-right media-middle"><i class="dic-more-vert dic"></i></div>
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Cerrar sesión</a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </li><!-- End user info -->
        {{-- Menu lateral ---}}
        @include('includes.menu-lateral',[ 'nombre'=> $nombre ])

    </ul>
</aside><!-- End Left sidebar -->
<!-- Sidebar Ends -->

<!--content area start-->
<div id="content" class="pmd-content inner-page">
    <!--tab start-->
    <div class="container-fluid full-width-container blank">
        <!-- Title -->
        <h2>{{$nombre}}</h2><!-- End Title -->
        {{-- contenido la plantilla --}}
        @yield('content')
    </div><!-- tab end -->

</div><!-- content area end -->


<!-- extras -->
@yield('extras')
<!-- Scripts Starts -->
<script src="{{asset('assets/js/jquery-1.12.2.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/propeller.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('components/select2/js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{asset('components/select2/js/pmd-select2.js')}}"></script>
<script>
    $(document).ready(function () {
        var sPath = window.location.pathname;
        var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
        $(".pmd-sidebar-nav").each(function () {
            $(this).find("a[href='" + sPage + "']").parents(".dropdown").addClass("open");
            $(this).find("a[href='" + sPage + "']").parents(".dropdown").find('.dropdown-menu').css("display", "block");
            $(this).find("a[href='" + sPage + "']").parents(".dropdown").find('a.dropdown-toggle').addClass("active");
            $(this).find("a[href='" + sPage + "']").addClass("active");
        });
        $(".auto-update-year").html(new Date().getFullYear());
    });
</script>
@yield('scripts')
<!-- Scripts Ends -->
</body>
<!-- Google icon -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</html>